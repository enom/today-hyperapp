// eslint-disable-next-line
import { h } from 'hyperapp'

import Clock from './clock/clock'
import utils from '../utils'

function ClockWidget ({ clock }) {
  utils.mustbe(clock, Clock)

  let { date } = clock
  let str = utils.phpize(date)

  return (
    <section class='clock container'>
      <div class='row align-items-center'>
        <div class='col'>
          <div class='clock-hour'>{ str.h }</div>
          <div class='clock-day-name'>{ str.l }</div>
          <div class='clock-day'>{ str.d }</div>
        </div>
        <div class='col-xs'>
          <div class='clock-divider-time'>:</div>
          <div class='clock-divider-date'>/</div>
        </div>
        <div class='col'>
          <div class='clock-minute'>{ str.i }</div>
          <div class='clock-month'>{ str.m }</div>
          <div class='clock-month-name'>{ str.F }</div>
        </div>
        { /*
        <div class="col">
          <div class="clock-second">{ str.s }</div>
        </div>
        */ }
      </div>
    </section>
  )
}

export default ClockWidget
