// Models
import utils from '../../utils'
import { Filter } from './interfaces'

function Event (data) {
  this.raw = data
  this.start = new Date(data.start.dateTime || data.start.date)
  this.summary = data.summary
}

function All () {
  this.run = () => true
}

function Day (from, to) {
  this.from = from
  this.to = to

  this.run = (result) => {
    utils.mustbe(result, Event)

    let { start } = result

    if (this.to === undefined) {
      return start > this.from
    } else {
      return start > this.from && start < this.to
    }
  }
}

function Today () {
  let from = new Date()
  from.setHours(0, 0, 0, 0)

  let to = new Date(from)
  to.setHours(23, 59, 59)

  return new Day(from, to)
}

function Tomorrow () {
  let from = new Date()
  from.setHours(0, 0, 0, 0)
  from.setDate(from.getDate() + 1)

  let to = new Date(from)
  to.setHours(23, 59, 59)

  return new Day(from, to)
}

function Someday () {
  const max = 8
  let count = 0
  let from = new Date()

  from.setHours(0, 0, 0, 0)
  from.setDate(from.getDate() + 2)

  let day = new Day(from)

  // Change filter to accept only 8 results
  this.run = (result) => {
    let parent = day.run(result)
    if (count > max) return false
    if (parent) count++
    return parent
  }
}

// All implements Filter
All.prototype = new Filter()

// Day implements Filter
Day.prototype = new Filter()

// Today extends Day
Today.prototype = new Day()

// Tomorrow extends Day
Tomorrow.prototype = new Day()

// Someday extends Day
Someday.prototype = new Day()

export {
  Event,
  All,
  Day,
  Today,
  Tomorrow,
  Someday
}
