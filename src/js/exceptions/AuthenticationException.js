function AuthenticationException (message, api) {
  this.api = api
  this.name = 'Authentication Exception'
  this.message = message
}

AuthenticationException.prototype = new Error

export default AuthenticationException
