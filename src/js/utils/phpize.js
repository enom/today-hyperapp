import en from './labels'
import daysin from './daysin'

/**
 * @see {@link http://php.net/manual/en/function.date.php}
 */
export default function phpize (date, labels = en) {
  const pad = s => s < 10 ? `0${s}` : `${s}`
  const str = s => `${s}`

  let year = date.getFullYear()
  let month = date.getMonth()
  let week = date.getDay()
  let day = date.getDate()
  let hour = date.getHours()
  let minute = date.getMinutes()
  let second = date.getSeconds()
  let ms = date.getMilliseconds()
  let pm = hour > 12
  let am = pm ? hour - 12 : hour

  return {
    d: pad(day),
    D: str(labels.day(date).substr(0, 3)),
    j: str(day),
    l: labels.day(date),
    N: str(week + 1),
    S: labels.suffix(date),
    w: str(week),
    z: str(daysin.to.days(date)),
    W: str(daysin.to.weeks(date)),
    F: labels.month(date),
    m: pad(month + 1),
    M: labels.month(date).substr(0, 3),
    n: str(month + 1),
    t: str(daysin.months[month]),
    L: null,
    o: null,
    Y: str(year),
    y: str(year).substr(2),
    a: pm ? 'pm' : 'am',
    A: pm ? 'PM' : 'AM',
    B: null,
    g: str(am),
    G: str(hour),
    h: pad(am),
    H: pad(hour),
    i: pad(minute),
    ii: str(minute),
    s: pad(second),
    ss: str(second),
    u: null,
    v: str(ms),
    e: null,
    I: null,
    O: null,
    P: null,
    T: null,
    Z: null,
    c: date.toISOString(),
    r: null,
    U: str(date.getTime())
  }
}
