// Component
import utils from '../../utils'

function Clock (date) {
  utils.mustbe(date, Date)

  this.date = date
  this.tick = (seconds = 1) => {
    this.date.setSeconds(this.date.getSeconds() + seconds)
  }
}

export default Clock
