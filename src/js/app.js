// Sass
import '../sass/app.sass'

// App
// eslint-disable-next-line
import { h, app } from 'hyperapp'

import AuthApi from './api/auth'
import CalendarApi from './api/calendar'
import WeatherApi from './api/weather'
import utils from './utils'

// Calendar components
import Calendar from './components/calendar/calendar'
import CalendarWidget from './components/CalendarWidget'
import { Event, Today, Tomorrow, Someday } from './components/calendar/models'

// Clock components
import Clock from './components/clock/clock'
import ClockWidget from './components/ClockWidget'

// Http components
import Http from './components/http/http'
import HttpWidget from './components/HttpWidget'

// Weather components
import Weather from './components/weather/weather'
import WeatherWidget from './components/WeatherWidget'
import { All, Forecast } from './components/weather/models'

import AuthWidget from './components/AuthWidget'

// Create APIs
const auth = new AuthApi()
const calendar = new CalendarApi({ auth })
const weather = new WeatherApi()

// Global state
const state = {
  http: new Http(auth, calendar, weather),
  clock: new Clock(new Date()),
  today: new Calendar(new Today()),
  tomorrow: new Calendar(new Tomorrow()),
  someday: new Calendar(new Someday()),
  weather: new Weather(new All())
}

// Global actions
const actions = {
  pong: () => (state) => ({ http: state.http }),
  boot: () => (state, actions) => {
    let every = {
      second: 1 * 1000,
      minute: 1 * 60 * 1000,
      hour: 1 * 60 * 60 * 1000
    }

    // Make http reactive
    state.http.pong(() => actions.pong)

    // Log into Google and attempt to fetch calendar events again
    auth.login().then(() => actions.fetch())

    // Initial load
    actions.fetch()

    // Set up timers
    setInterval(actions.tick, every.minute)
    setInterval(actions.fetch, every.hour)
  },
  tick: () => (state) => ({
    clock: (state.clock.tick(60), state.clock)
  }),
  results: () => (state) => ({
    today: (state.today.load(calendar.results()), state.today),
    tomorrow: (state.tomorrow.load(calendar.results()), state.tomorrow),
    someday: (state.someday.load(calendar.results()), state.someday),
    weather: (state.weather.load(weather.results()), state.weather)
  }),
  fetch: () => (state, actions) => {
    Promise.all([
      weather.fetch((data) => new Forecast(data)),
      calendar.fetch((data) => new Event(data))
    ]).then(() => {
      actions.results()
    }).catch((error) => {
      throw error
    })
  }
}

// Global view
const view = (state, actions) => (
  <div id='app' class='container-fluid'>
    <div className='row'>
      <div className='col-5'>

        { /* --- CLOCK ----------------------------------------------------- */ }

        <ClockWidget clock={state.clock} />

        { /* --- WEATHER --------------------------------------------------- */ }

        <WeatherWidget weather={state.weather} />

      </div>
      <div className='col'>

        { /*  -- AUTHENTICATION ------------------------------------------- */ }

        <AuthWidget auth={auth} />

        { /*  -- TODAY ---------------------------------------------------- */ }

        <h2>
          Today
          <small>
            <span>/</span>
            <span>{ utils.labels.day(state.today.date) }</span>
            <span>{ utils.labels.month(state.today.date) }</span>
            <span>{ state.today.date.getDate() }</span>
          </small>
        </h2>

        <CalendarWidget calendar={state.today} />

        { /* --- TOMORROW ------------------------------------------------- */ }

        <h2>
          Tomorrow
          <small>
            <span>/</span>
            <span>{ utils.labels.day(state.tomorrow.date) },</span>
            <span>{ utils.labels.month(state.tomorrow.date) }</span>
            <span>{ state.tomorrow.date.getDate() }</span>
          </small>
        </h2>

        <CalendarWidget calendar={state.tomorrow} />

        { /* --- SOMEDAY -------------------------------------------------- */ }

        <h2>Someday</h2>

        <CalendarWidget calendar={state.someday} hours={false} />

      </div>
    </div>

    { /* --- HTTP ---------------------------------------------------------- */ }

    <HttpWidget http={state.http} />

    { /* Hides mouse cursor */ }

    <div id="mouse"></div>
  </div>
)

// Mount and boot
app(state, actions, view, document.body).boot()
