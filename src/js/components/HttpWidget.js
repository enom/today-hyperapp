// eslint-disable-next-line
import { h } from 'hyperapp'

import Http from './http/http'
import utils from '../utils'

function Status({ name, state }){
  return (
    <div class='status-{name}'>{name}:{state}</div>
  )
}

function HttpWidget ({ http }) {
  utils.mustbe(http, Http)

  let { last } = http
  let str = utils.phpize(last)

  const make = (state) => ({ name }) => Status({ name, state })

  return (
    <div class='status'>
      <div class='status-time'>{ str.H }:{ str.i }:{ str.s }</div>
      <div class='status-date'>{ str.d }/{ str.m }/{ str.y }</div>
      { http.pending.map(make('pending')) }
      { http.success.map(make('success')) }
      { http.fail.map(make('fail')) }
    </div>
  )
}

export default HttpWidget
