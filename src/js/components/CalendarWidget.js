// eslint-disable-next-line
import { h } from 'hyperapp'

import Calendar from './calendar/calendar'
import { Event } from './calendar/models'
import utils from '../utils'

function EventWidget ({ event, hours }) {
  utils.mustbe(event, Event)

  let { start, summary } = event
  let str = utils.phpize(start)
  let time = `${str.H}h${str.i}`

  return (
    <div class='row event'>
      { hours ? (
        <div class='col-3 event-time'>
          { time.split('').map((letter) => (
            <span>{ letter }</span>
          ))}
        </div>
      ) : (
        <div class='col-3 event-date'>
          { str.F } { str.j }{ str.S }
        </div>
      ) }

      <div class='col event-summary'>{ summary }</div>
    </div>
  )
}

function CalendarWidget ({ calendar, hours = true }) {
  utils.mustbe(calendar, Calendar)

  let { events } = calendar

  return (
    events.length ? events.map((event) => (
      <EventWidget event={event} hours={hours} />
    )) : (
      <div class='row event-none'>
        <div class='col'>No events</div>
      </div>
    )
  )
}

export default CalendarWidget
export {
  EventWidget
}
