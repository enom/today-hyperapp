// eslint-disable-next-line
import { h } from 'hyperapp'

import Weather from './weather/weather'
import { Forecast } from './weather/models'
import utils from '../utils'

function ForecastWidget ({ forecast }) {
  utils.mustbe(forecast, Forecast)

  let { time, feel, temp, rain } = forecast

  time = `${time}h00`

  return (
    <div class='row temp'>
      <div class='col temp-time'>
        { time.split('').map((letter) => (
          <span>{ letter }</span>
        )) }
      </div>

      <div class='col temp-temp'>
        <span class='temp-feel'>{ feel }&deg;</span>
        <span class='temp-divider'>/</span>
        <span class='temp-actual'>{ temp }&deg;</span>
      </div>

      <div class='col temp-rain'>
        { rain }%
      </div>
    </div>
  )
}

function WeatherWidget ({ weather }) {
  utils.mustbe(weather, Weather)

  let { forecasts } = weather

  return (
    <section class='weather'>
      { forecasts.length ? forecasts.map((forecast) => (
        <ForecastWidget forecast={forecast} />
      )) : (
        <div class='row weather-none'>
          <div class='col'>No forecast</div>
        </div>
      ) }
    </section>
  )
}

export default WeatherWidget
export {
  ForecastWidget
}
