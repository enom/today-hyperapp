// Libraries
import axios from 'axios'

// Credentials via alias
import { wu as env } from 'berry-env'

// Interfaces
import Api from './api'

const API_KEY = env.api_key
const API_URL = `https://api.wunderground.com/api/${API_KEY}/hourly/q/Canada/Ottawa.json`
const HOURS = 10
const cors = {
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json'
  }
}

// Local state
const state = {
  results: []
}

function weather () {
  this.name = 'weather'

  this.results = () => state.results
  this.fetch = (makeable) => {
    const executor = (resolve, reject) => {
      axios.get(API_URL, cors).then((response) => {
        let data = response.data.hourly_forecast.slice(0, HOURS)
        let results = makeable ? data.map(makeable) : data

        resolve(state.results = results)
      }, reject).catch(reject)
    }

    return new Promise(executor)
  }
}

// weather implements api
weather.prototype = new Api()

export default weather
